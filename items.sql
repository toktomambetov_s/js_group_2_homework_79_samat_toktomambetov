USE `homework_79`;
CREATE TABLE `items` (
	`id` INT NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(255) NOT NULL,
    `category_id` INT NOT NULL,
    `location_id` INT NOT NULL,
    `description` TEXT,
    `date` DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (`id`),
    INDEX `FK_category_idx` (`category_id`),
    CONSTRAINT `FK_category`
		FOREIGN KEY (`category_id`)
        REFERENCES `categories` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE,
	INDEX `FK_location_idx` (`location_id`),
    CONSTRAINT `FK_location`
		FOREIGN KEY (`location_id`)
        REFERENCES `locations` (`id`)
        ON DELETE RESTRICT
        ON UPDATE CASCADE
);